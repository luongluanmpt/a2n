# A2N



## Mô tả

Tình huống đặt ra là bạn được giao việc viết hàm cài đặt thuật toán cộng 2 số lớn (được biểu diễn dưới dạng chuỗi) với thuật toán như học sinh Tiểu học
đã làm.

Hàm này sẽ được đóng gói để bàn giao cho một nhóm khác làm giao diện (hoặc ứng dụng dạng console) để họ gọi hàm của bạn trong dự án lớn hơn.

## Yêu cầu

Viết phần lõi (core) trong 1 lớp riêng "MyBigNumber", method String sum(String stn1, String stn2) để cài đặt thuật toán cộng 2 số giống như các học sinh Tiểu học
thực hiện như sau:

Duyệt đồng thời chuỗi stn1, stn2 từ phải sang trái, lấy ra từng kí tự (character), chuyển thành kí số (digit).
Cộng từng kí số.

Ghi nhận lại lịch sử phép toán vừa thực hiện (Ưu tiên dùng LOGGING. Không biết logging thì dùng PRINT cũng tạm chấp nhận).

...

Ví dụ lệnh sum("1234", "897") sẻ được thực hiện như sau:
Bước 1: Lấy 4 cộng với 7 được 11.
Lưu 1 vào kết quả và nhớ 1.
Bước 2: Lấy 3 cộng với 9 được 12. Cộng tiếp với nhớ 1 được 13
Lưu 3 vào kết quả được kết quả mới là "31"

...

## Run project
Project sử dụng Maven để build [Maven](https://maven.apache.org/)

Cài đặt môi trường: Java 11 [Tại đây](https://www.oracle.com/eg/java/technologies/javase/jdk11-archive-downloads.html)

Tool: Tải IDE IntelliJ: [Tại đây](https://www.jetbrains.com/idea/download/)

B1: Tải project
```
git clone https://gitlab.com/luongluanmpt/a2n.git
```
B2: Mở project trong IntelliJ

B3: Vào File src/test/java/MyBigNumberTest và chọn Run