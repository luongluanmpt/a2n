import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class MyBigNumber {

    private static final Logger LOGGER = LogManager.getLogger(MyBigNumber.class);

    public static String sum(String stn1, String stn2) {
        String result = "";
        // xác định độ đài của 2 chuỗi
        int len1 = stn1.length();
        int len2 = stn2.length();
        // xác định độ dài tối đa của 2 chuỗi
        int maxLen = Math.max(len1, len2);
        char c1; // kí tự cuối của chuỗi 1
        char c2; // kí tự cuối của chuỗi 2
        int d1; // giá trị của kí tự cuối chuỗi 1
        int d2; // giá trị của kí tự cuối chuỗi 2
        int total; // tổng của giá trị 2 chữ số cuối
        int mem = 0; // biến nhớ
        int temp; // giá kết quả tạm thời
        LOGGER.info(String.format("==============(%s + %s)==============", stn1, stn2));
        // Duyệt các kí tự từ phải qua trái
        for (int i = 0; i < maxLen; i++) {
            int i1 = len1 - 1 - i; // chỉ số index chuỗi 1
            int i2 = len2 - 1 - i; // chỉ số index chuỗi 2
            c1 = '0';
            c2 = '0';
            // lấy kí tự cuối của chuỗi 1 và chuỗi 2
            if (i1 >= 0) {
                c1 = stn1.charAt(i1);
            }
            if (i2 >= 0) {
                c2 = stn2.charAt(i2);
            }
            // chuyển sang kiểu số
            d1 = c1 - '0';
            d2 = c2 - '0';
            // tính tổng
            total = d1 + d2 + mem;
            LOGGER.info(String.format("Lấy %d cộng với %d được %d.", d1, d2, d1 + d2));
            if (mem != 0) {
                LOGGER.info(String.format(" Cộng tiếp với nhớ %d được %d.", mem, total));
            }
            // lấy phần dư và phần nguyên
            temp = total % 10;
            mem = total / 10;
            if (mem != 0) {
                LOGGER.info(String.format("Ghi nhớ %d.", mem));
            }
            // lưu vào kết quả
            result = temp + result;
            LOGGER.info(String.format("Lưu %d vào kết quả được kết quả mới là \"%s\".", temp, result));
        }
        if (mem != 0) {
            result = mem + result;
            LOGGER.info(String.format("Lấy %d cộng với %d được %d.", mem, 0, mem));
            LOGGER.info(String.format("Lưu %d vào kết quả được kết quả mới là \"%s\".", mem, result));
        }
        LOGGER.info("Kết quả: " + result);
        LOGGER.info("==============END==============");
        return result;
    }

}
