import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyBigNumberTest {
    public MyBigNumber myBigNumber = new MyBigNumber();

    @Test
    public void test1() {
        assertEquals(myBigNumber.sum("1234", "897"),"2131");
    }

    @Test
    public void test2() {
        assertEquals(myBigNumber.sum("897", "1234"),"2131");
    }

    @Test
    public void test3() {
        assertEquals(myBigNumber.sum("1234", "1234"),"2468");
    }

    @Test
    public void test4() {
        assertEquals(myBigNumber.sum("1234", "0"),"1234");
    }

}